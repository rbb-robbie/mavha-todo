<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodoTaskAttachmentRepository")
 */
class TodoTaskAttachment extends Attachment
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TodoTask", inversedBy="attachments", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $todoTask;

    public function getTodoTask(): ?TodoTask
    {
        return $this->todoTask;
    }

    public function setTodoTask(?TodoTask $todoTask): self
    {
        $this->todoTask = $todoTask;

        return $this;
    }
}
