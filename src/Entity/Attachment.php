<?php

namespace App\Entity;

use App\Entity\Decorator\Timestamp;
use App\Entity\Decorator\UserScope;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
abstract class Attachment
{

    use Timestamp;
    use UserScope;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @Vich\UploadableField(mapping="attachments", fileNameProperty="attachmentName", size="attachmentSize")
     *
     * @var File
     */
    protected $attachmentFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $attachmentName;

    /**
     * @ORM\Column(type="integer")
     */
    protected $attachmentSize;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $attachmentFile
     */
    public function setAttachmentFile(?File $attachmentFile = null): void
    {
        $this->attachmentFile = $attachmentFile;

        if ($attachmentFile !== null) {
            $this->updatedAt();
        }
    }
    public function getAttachmentFile(): ?File
    {
        return $this->attachmentFile;
    }

    public function getAttachmentName(): ?string
    {
        return $this->attachmentName;
    }

    public function setAttachmentName(string $attachmentName): self
    {
        $this->attachmentName = $attachmentName;

        return $this;
    }

    public function getAttachmentSize(): ?int
    {
        return $this->attachmentSize;
    }

    public function setAttachmentSize(int $attachmentSize): self
    {
        $this->attachmentSize = $attachmentSize;

        return $this;
    }

}
