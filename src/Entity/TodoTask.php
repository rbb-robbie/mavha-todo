<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use App\Entity\Decorator\Timestamp;
use App\Entity\Decorator\UserScope;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodoTaskRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TodoTask
{

    use Timestamp;
    use UserScope;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="TodoTaskStatus")
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TodoTaskAttachment", mappedBy="todoTask", orphanRemoval=true)
     */
    private $attachments;

    public function __construct()
    {
        $this->attachments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|TodoTaskAttachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(TodoTaskAttachment $attachment): self
    {
        if (!$this->attachments->contains($attachment)) {
            $this->attachments[] = $attachment;
            $attachment->setTodoTask($this);
        }

        return $this;
    }

    public function removeAttachment(TodoTaskAttachment $attachment): self
    {
        if ($this->attachments->contains($attachment)) {
            $this->attachments->removeElement($attachment);
            // set the owning side to null (unless already changed)
            if ($attachment->getTodoTask() === $this) {
                $attachment->setTodoTask(null);
            }
        }

        return $this;
    }


}
