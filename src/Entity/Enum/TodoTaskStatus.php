<?php
namespace App\Entity\Enum;

class TodoTaskStatus extends EnumType {
    protected $name = "TodoTaskStatus";
    protected $values = array("PENDING","DONE");
}