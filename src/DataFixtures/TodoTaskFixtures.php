<?php

namespace App\DataFixtures;

use App\Entity\TodoTask;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TodoTaskFixtures extends BaseFakeableFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {
        $users = array(
            $this->getReference(UserFixtures::TEST_USER_A),
            $this->getReference(UserFixtures::TEST_USER_B),
            $this->getReference(UserFixtures::TEST_USER_C)
        );
        $status = array("PENDING", "DONE");
        $counter = 200;
        while($counter > 0){
            $todo = new TodoTask();
            $todo->setUser($users[rand(0,2)]);
            $todo->setDescription($this->faker->sentence(10, true));
            $todo->setStatus($status[rand(0,1)]);
            $manager->persist($todo);
            $counter--;
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
     }
}
