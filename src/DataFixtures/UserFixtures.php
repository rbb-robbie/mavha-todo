<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const TEST_USER_A = 'test-user-1';
    public const TEST_USER_B = 'test-user-2';
    public const TEST_USER_C = 'test-user-3';

    private $passwordEncoder;
    private $users;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $persistedUsers = array();
        $this->initUsers();
        foreach ($this->users as $key => $u) {
            $user = new User();
            $user->setEmail($u["email"]);
            $user->setRoles($u["roles"]);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $u["password"]
            ));
            $user->setDeletedAt(null);
            $persistedUsers[] = $user;
            $manager->persist($user);
        }
        $this->addReference(self::TEST_USER_A, $persistedUsers[0]);
        $this->addReference(self::TEST_USER_B, $persistedUsers[1]);
        $this->addReference(self::TEST_USER_C, $persistedUsers[2]);
        $manager->flush();
    }

    private function initUsers()
    {
        $this->users = [
            [
                "email" => "admin@robbie.com",
                "roles" => ["ROLE_USER"],
                "password" => "robbiesformavha"
            ],
            [
                "email" => "tester@mavha.com",
                "roles" => ["ROLE_USER"],
                "password" => "testing"
            ],
            [
                "email" => "tester2@mavha.com",
                "roles" => ["ROLE_USER"],
                "password" => "testing"
            ]
        ];
        return $this;
    }
}
