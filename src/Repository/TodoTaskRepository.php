<?php

namespace App\Repository;

use App\Entity\TodoTask;
use App\Repository\Decorator\ContextualRepository;
use App\Repository\Decorator\ContextualRepositoryInterface;
use App\Repository\Decorator\FilterByParams;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;

use App\Repository\Decorator\ScopeByUser;

/**
 * @method TodoTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method TodoTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method TodoTask[]    findAll()
 * @method TodoTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodoTaskRepository extends ServiceEntityRepository implements ContextualRepositoryInterface
{
    use ScopeByUser;
    use ContextualRepository;
    use FilterByParams;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TodoTask::class);
    }

    public function findAllByContext($filters)
    {
        $qb = $this->createQueryBuilder('t');
        $this->filterByParams($qb, 't', $filters);
        $this->scopeByUser($qb, 't');
        return $qb
            ->setParameter("user", $this->getContext('user'))
            ->getQuery()
            ->execute();
    }

    public function findById($id)
    {
        $qb = $this->createQueryBuilder("t")
            ->where("t.id = :id")
            ->setParameter(":id", $id);
        $this->scopeByUser($qb, 't');
        $result = $qb
            ->setParameter("user", $this->getContext('user'))
            ->getQuery()
            ->execute();
        if (count($result)) {
            return $result[0];
        } else {
            return null;
        }
    }
}
