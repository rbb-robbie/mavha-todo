<?php
namespace App\Repository\Decorator;

trait ContextualRepository {
    protected $context = array();

    public function addToContext($key, $value){
        $this->context[$key] = $value;
    }
    public function getContext($key){
        return $this->context[$key];
    }
}