<?php

namespace App\Repository\Decorator;

use Doctrine\ORM\QueryBuilder;

trait FilterByParams
{
    protected function filterByParams(QueryBuilder $qb, string $ref, array $params)
    {
        foreach ($params as $key => $filter) {
            $filterVal = $filter['value'];
            if ($filterVal) {
                switch ($filter['type']) {
                    case "EQUALS":
                        $this->filterEqual($qb, $ref, $key, $filterVal);
                        return;
                    case "HAS":
                        $this->filterLike($qb, $ref, $key, $filterVal);
                    default:
                        return;
                }
            }
        }
    }

    private function filterEqual($qb, $ref, $key, $val)
    {
        $qb->andWhere($ref.'.'.$key.' = :value')->setParameter('value', $val);
    }

    private function filterLike($qb, $ref, $key, $val)
    {
        $qb->andWhere($ref.'.'.$key.' LIKE :value')->setParameter('value', '%' . $val . '%');
    }
}
