<?php

namespace App\Repository\Decorator;

use Doctrine\ORM\QueryBuilder;

trait ScopeByUser {
    protected $user;

    protected function scopeByUser(QueryBuilder $qb, String $ref){
        return $qb->andWhere($ref . '.user = :user');
    }
}