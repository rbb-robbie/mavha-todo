<?php
namespace App\Repository\Decorator;

interface ContextualRepositoryInterface {
    public function addToContext($key, $value);
    public function getContext($key);
}