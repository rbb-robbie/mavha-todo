<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;

use App\Entity\TodoTask;
use App\Entity\TodoTaskAttachment;
use App\Repository\TodoTaskAttachmentRepository;
use App\Repository\TodoTaskRepository;
use App\Representation\TodoTasksRepresentation;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;

class TodoTasksController extends AbstractFOSRestController
{

    private $em;
    private $repository;
    private $attachmentsRepository;

    public function __construct(TodoTaskRepository $repository, TodoTaskAttachmentRepository $attachmentsRepository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->attachmentsRepository = $attachmentsRepository;
        $this->em = $em;
    }

    /**
     * @Rest\QueryParam(name="id", nullable=true)
     * @Rest\QueryParam(name="description", nullable=true)
     * @Rest\QueryParam(name="status", nullable=true)
     */
    public function getTodosAction(ParamFetcher $paramFetcher)
    {
        $this->repository->addToContext("user", $this->getUser());
        $filteredSearch = $paramFetcher->get("id") || $paramFetcher->get("description") || $paramFetcher->get("status");
        $filters = array(
            "id" => array(
                "value" => $paramFetcher->get("id"),
                "type" => "HAS"
            ),
            "description" => array(
                "value" => $paramFetcher->get("description"),
                "type" => "HAS"
            ),
            "status" => array(
                "value" => $paramFetcher->get("status"),
                "type" => "EQUALS"
            )
        );
        $raw = $this->repository->findAllByContext($filters);
        if (count($raw) || $filteredSearch) {
            $view = new TodoTasksRepresentation($raw);
            return $this->view(array(
                "status" => Response::HTTP_OK,
                "data" => $view->getRepresentation()
            ));
        } else {
            return $this->view(
                array(
                    "status" => Response::HTTP_NOT_FOUND,
                    "data" => []
                ),
                Response::HTTP_NOT_FOUND
            );
        }
    }

    public function getTodoAction(int $id)
    {
        $this->repository->addToContext("user", $this->getUser());
        $raw = $this->repository->findOneById($id);
        if ($raw) {
            $view = new TodoTasksRepresentation($raw);
            return $this->view(array(
                "status" => Response::HTTP_OK,
                "data" => $view->getRepresentation()
            ));
        } else {
            return $this->view(
                array(
                    "status" => Response::HTTP_NOT_FOUND,
                    "data" => []
                ),
                Response::HTTP_NOT_FOUND
            );
        }
    }

    public function deleteTodosAction(int $id)
    {
        $this->repository->addToContext("user", $this->getUser());
        $raw = $this->repository->findOneById($id);
        if ($raw) {
            $this->em->remove($raw);
            $this->em->flush();
            return $this->view(array(
                "status" => Response::HTTP_OK,
                "data" => []
            ));
        } else {
            return $this->view(
                array(
                    "status" => Response::HTTP_NOT_FOUND,
                    "data" => []
                ),
                Response::HTTP_NOT_FOUND
            );
        }
    }

    /**
     * @Rest\RequestParam(name="description", description="Task To-Do", nullable=false)
     * @Rest\FileParam(name="attachment", description="The Todo Attachment", nullable=true)
     * @param ParamFetcher $paramFetcher;
     */
    public function postTodosAction(ParamFetcher $paramFetcher)
    {
        $todo = new TodoTask();
        $todo->setDescription($paramFetcher->get('description'));
        $todo->setStatus("PENDING");
        $todo->setUser($this->getUser());

        $file = $paramFetcher->get('attachment');
        if ($file) {
            $attachment = new TodoTaskAttachment();
            $attachment->setAttachmentFile($file);
            $attachment->setUser($this->getUser());
            $this->em->persist($attachment);
            $todo->addAttachment($attachment);
        }

        $this->em->persist($todo);
        $this->em->flush();
        $view = new TodoTasksRepresentation($todo);
        return $this->view(array(
            "status" => Response::HTTP_CREATED,
            "data" => $view->getRepresentation()
        ));
    }

    /**
     * @Rest\RequestParam(name="description", description="Task To-Do", nullable=true)
     * @Rest\FileParam(name="attachment", description="The Todo Attachment", nullable=true)
     * @param ParamFetcher $paramFetcher;
     */
    public function postTodoAction(int $id, ParamFetcher $paramFetcher)
    {
        $this->repository->addToContext("user", $this->getUser());
        $todo = $this->repository->findById($id);
        $description = $paramFetcher->get('description');
        $file = $paramFetcher->get('attachment');

        if ($description) {
            $todo->setDescription($paramFetcher->get('description'));
        }
        if ($file) {
            $attachment = new TodoTaskAttachment();
            $attachment->setAttachmentFile($file);
            $attachment->setUser($this->getUser());
            $this->em->persist($attachment);

            $todo->addAttachment($attachment);
        }
        $this->em->persist($todo);
        $this->em->flush();
        $view = new TodoTasksRepresentation($todo);
        return $this->view(array(
            "status" => Response::HTTP_OK,
            "data" => $view->getRepresentation()
        ));
    }

    /**
     * @Rest\FileParam(name="attachment", default="noPicture")
     */
    public function postTodoAttachmentAction(int $todo_id, ParamFetcher $paramFetcher)
    {
        $this->repository->addToContext("user", $this->getUser());
        $todo = $this->repository->findById($todo_id);

        $file = $paramFetcher->get('attachment');
        dump($paramFetcher);
        if ($file) {
            $attachment = new TodoTaskAttachment();
            $attachment->setAttachmentFile($file);
            $attachment->setUser($this->getUser());
            $this->em->persist($attachment);

            $todo->addAttachment($attachment);
        }
        $this->em->persist($todo);
        $this->em->flush();
        $view = new TodoTasksRepresentation($todo);
        return $this->view(array(
            "status" => Response::HTTP_CREATED,
            "data" => $view->getRepresentation()
        ));
    }

    public function deleteTodosAttachmentAction(int $todo_id, int $id)
    {
        $attachment = $this->attachmentsRepository->findOneById($id);
        // TO-DO: Implement User Scope for Attachments
        if ($attachment && $attachment->getUser() === $this->getUser()) {
            $this->em->remove($attachment);
            $this->em->flush();
            return $this->view(array(
                "status" => Response::HTTP_OK,
                "data" => []
            ));
        } else {
            return $this->view(array(
                "status" => Response::HTTP_NOT_FOUND,
                "data" => []
            ));
        }
    }

    /**
     * @Rest\RequestParam(name="status", description="Status", nullable=true)
     */
    public function patchTodosStatusAction(int $id, ParamFetcher $paramFetcher)
    {
        $this->repository->addToContext("user", $this->getUser());
        $todo = $this->repository->findById($id);
        if ($todo) {
            $todo->setStatus($paramFetcher->get('status'));
            $this->em->persist($todo);
            $this->em->flush();

            $view = new TodoTasksRepresentation($todo);
            return $this->view(array(
                "status" => Response::HTTP_OK,
                "data" => $view->getRepresentation()
            ));
        } else {
            return $this->view(
                array(
                    "status" => Response::HTTP_NOT_FOUND,
                    "data" => []
                ),
                Response::HTTP_NOT_FOUND
            );
        }
    }
}
