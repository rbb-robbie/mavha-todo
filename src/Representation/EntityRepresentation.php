<?php

namespace App\Representation;

abstract class EntityRepresentation
{
    protected $raw;
    public function __construct($data)
    {
        $this->raw = $data;
    }
    public function getRepresentation()
    {
        return $this->build();
    }
    protected function build()
    {
        $response = null;
        if (is_array($this->raw) || is_a($this->raw, "Doctrine\ORM\PersistentCollection")) {
            $response = array();
            $raw = $this->raw;
            if(is_a($this->raw, "Doctrine\ORM\PersistentCollection")){
                $raw = $this->raw->getValues();
            }
            foreach ($raw as $row) {
                array_push($response, $this->representation($row));
            }
        } else {
            $response = $this->representation($this->raw);
        }
        return $response;
    }

    abstract protected function representation($entity);
}
