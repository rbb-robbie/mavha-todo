<?php

namespace App\Representation;

class TodoTaskAttachmentsRepresentation extends EntityRepresentation {
    protected function representation($entity){
        return array(
            "id" => $entity->getId(),
            "filename" => $entity->getAttachmentName(),
            "path" => $entity->getAttachmentFile()->getPath(),
            "filepath" => $entity->getAttachmentFile()->getRealPath(),
            "size" => $entity->getAttachmentSize(),
        );
    }
}