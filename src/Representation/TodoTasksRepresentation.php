<?php

namespace App\Representation;

class TodoTasksRepresentation extends EntityRepresentation{
    protected function representation($entity){
        $attachments = new TodoTaskAttachmentsRepresentation($entity->getAttachments());
        return array(
            "id" => $entity->getId(),
            "description" => $entity->getDescription(),
            "status" => $entity->getStatus(),
            "attachments" => $attachments->getRepresentation()
        );
    }
}